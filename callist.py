from app import app

from gevent.pywsgi import WSGIServer
import ssl

http_server = WSGIServer(('', 5005), app)
http_server.serve_forever()

