from app import app
from flask import render_template

import caldav
import datetime
from collections import OrderedDict
import locale
locale.setlocale(locale.LC_ALL, "de_DE.utf8")
import csv
import requests


@app.route('/')
@app.route('/index')
def index():

    class ics_event():
        def __init__(self, eventstr):

            classes = ["primary", "secondary", "success", "danger", "warning", "info", "light", "dark" ,"white" ,"active"]
            class_style = "table-"

            for i,c in enumerate(classes):
                classes[i] = f"{class_style}{c}"

            # STATUS is mostly just set for status "CANCELLED" or "TENTATIV"
            setattr(self, 'STATUS', 'CONFIRMED')
				
            # self.category = classes[2]
            lines = eventstr.replace('\n ','').splitlines() #re.sub(r'\n ', '', eventstr).splitlines()
            vevent = 0
            for l in lines:
                if "BEGIN:VEVENT" in l: vevent = 1
                elif "END:VEVENT" in l: vevent = 0
                if not vevent: continue
                key = l.split(":")[0].split(";")[0]
                val = l.split(":",1)[1]
                keep_key = ('DTSTART', 'DTEND', 'SUMMARY', 'CATEGORIES', 'DESCRIPTION', 'LOCATION', 'STATUS')
                
                if key in keep_key:
                    if key == "SUMMARY" or key == "LOCATION" or key == "DESCRIPTION":
                        val = val.replace('\,',',').replace('\;',';')
                    elif key == "DTSTART" or key == "DTEND":
                        if not "VALUE=DATE" in l.split(":")[0]:
                            self.FULLDAY = False
                            val = datetime.datetime.strptime(val, '%Y%m%dT%H%M%S')
                            if key == "DTSTART":
                                self.startday = val.date()
                                self.starttimestr = val.strftime("%H:%M")
                            elif key == "DTEND":
                                self.endday = val.date()
                                self.endtimestr = val.strftime("%H:%M")
                        else:
                            self.FULLDAY = True
                            val = datetime.datetime.strptime(val, '%Y%m%d').date()
                            if key == "DTSTART":
                                self.startday = val
                            elif key == "DTEND":
                                self.endday = val - datetime.timedelta(days = 1)
                    elif key == "CATEGORIES":
                        # map categories to a corresponding string from classes in self.category (and use this in template)
                        # if val == "Abgabe": self.category = classes[3]
                        # elif val == "Eltern": self.category = classes[9]
                        # elif val == "Exkursion": self.category = classes[2]
                        # elif val == "Extern": self.category = classes[6]
                        # elif val == "Ferien": self.category = classes[1]
                        # elif val == "Klausur": self.category = classes[4]
                        # elif val == "Konferenz": self.category = classes[0]
                        # elif val == "Projekt": self.category = classes[5]
                        val = None

                    setattr(self, key, val)
            setattr(self, 'repetition_days', (self.endday - self.startday).days + 1)
            # events with more than one day should appear at the top even if they are not marked with "VALUE=DATE"
            if self.repetition_days > 1: self.FULLDAY = True 

        def is_at(self, d):
            # check, if this date is on d (or not)
            dtest = d
            if self.startday <= dtest <= self.endday:
                return True
            else:
                return False

        def summary_ext(self, dtest):
            # extend SUMMARY with (i/n) for repeated events
            sum_ext_head = ""
            sum_ext_tail = ""
            if hasattr(self, "LOCATION"): sum_ext_tail = f" [{self.LOCATION}]"
            if self.repetition_days > 1:
                repetition = (dtest - self.startday).days + 1
                if hasattr(self, "starttimestr") and repetition == 1: sum_ext_head = f"ab {self.starttimestr} "
                if hasattr(self, 'endtimestr') and repetition == self.repetition_days: sum_ext_head = f"bis {self.endtimestr} "
                sum_ext_tail =  f"{sum_ext_tail} ({repetition}/{self.repetition_days})"
            return f"{sum_ext_head}{self.SUMMARY}{sum_ext_tail}"
 
        def add_description(self):
            if hasattr(self, 'DESCRIPTION'):
                return f"{self.DESCRIPTION}"
            else:
                return ""

    class Date_Header():
        def __init__(self, hclasses, show = 0, title = "", hclassnr = 0):
            # if show = 0 date header is only shown in the case there is an event, show = 1 means it is shown anyway
            self.show = show
            self.title = title
            self.hclass = hclasses[hclassnr]

    def is_conference_day(date, conference_days):
        for tt in conference_days:
            if datetime.date(tt[0][0],tt[0][1], tt[0][2]) < date < datetime.date(tt[1][0],tt[1][1], tt[1][2]):
                if date.weekday() == tt[2]:
                    return True
        return False


    def daterange(start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + datetime.timedelta(n)


    def get_schoolyear_start():
        # returns the starting year of the actual school year. start of summer holidays is used as deadline

        # certain times where this does not apply
        scy_end_after = datetime.datetime.strptime("15.06.", "%d.%m.").timetuple().tm_yday
        scy_end_before = datetime.datetime.strptime("15.08.", "%d.%m.").timetuple().tm_yday
        url_base = "https://www.ferienwiki.de/exports/ferien/yyyy/de/niedersachsen"
        date_filename = 'schoolyear_start.txt'

        year = datetime.datetime.now().year
        url = url_base.replace('yyyy', str(year))
        day_of_year = datetime.datetime.now().timetuple().tm_yday

        if day_of_year < scy_end_after:
            return year - 1
        elif day_of_year > scy_end_before:
            return year

        try:
            with open(date_filename, 'r+') as file:
                sy_end_str = str(file.read()).rstrip()
        except FileNotFoundError:
            sy_end_str = ''

        if sy_end_str:
            sy_end = datetime.datetime.strptime(sy_end_str, "%Y%m%d")
        else:
            sy_end = ""

        if sy_end == "" or datetime.datetime.now().year > sy_end.year:
            rsp = requests.get(url)
            for ev in rsp.text.split("BEGIN:VEVENT"):
                sf = 0
                # file uses DOS-LB "\r\n"
                for r in ev.split("\r\n"):
                    if r.split(":")[0].split(";")[0] == "DTSTART":
                        sy_end_str = r.split(":")[1]
                        sy_end = datetime.datetime.strptime(sy_end_str, "%Y%m%d")
                    elif r.split(":")[0] == "SUMMARY" and r.split(":")[1] == "Sommerferien":
                        sf = 1
                if sf == 1:
                    break
            with open(date_filename, 'w+') as file:
                file.write('{}'.format(sy_end_str))
    
        if datetime.datetime.now() < sy_end:
            return (year - 1)
        else:
            return year

    def get_Date_Header_objs(conf_day, classes):
        # read the info from csv-files: ferien_NI.csv and feiertage_NI.csv
        # holidays will be read to tuple: (name, start, ende)
        holidays = []
        with open("ferien_NI.csv",'r',newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                holidays.append((row[0], datetime.datetime.strptime(row[1], "%Y%m%d").date(), datetime.datetime.strptime(row[2], "%Y%m%d").date()))
        # free days
        freedays = dict()
        with open("feiertage_NI.csv",'r',newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                date = datetime.datetime.strptime(row[1], "%Y%m%d").date()
                freedays[date] = row[0]
        # get startdate (last day of summer holidays of the current schoolyear) und enddate (end of summerholidays of the next schoolyear)
        sy = get_schoolyear_start()
        shlds = tuple()
        i = 0
        for hd in holidays:
            if hd[0] == "Sommerferien":
                if hd[1].year == sy + i:
                    shlds = shlds + ((hd[1], hd[2]),)
                    i += 1

        # beginning with the schoolyears start up to new year only the current schoolyear will be analyzed
        show_year = 1 if sy == datetime.datetime.now().year else 2
        kw = 0
        sw = 0
        date_header_obj = dict()
        # all days of the daterange to show will be looped
        for d in daterange(shlds[0][1], shlds[show_year][1] + datetime.timedelta(days=1)): #min(show_year, len(shlds))
            # no text for saturdays and sundays
            if d.weekday() > 4:
                date_header_obj[d] = Date_Header(classes)
                continue
            # first or last day of holidays? holiday-day?
            for hd in holidays:
                stop = 0
                # first day of holidays?
                if d == hd[1]:
                    if d == hd[2]:
                        title = f' - {hd[0]}'
                    else:
                        title = f" - Beginn {hd[0]}"
                    date_header_obj[d] = Date_Header(classes, show = 1, title = title, hclassnr = 1)
                    stop = 1
                    break
                # last day of holidays?
                if d == hd[2]:
                    if d != hd[1]:
                        date_header_obj[d] = Date_Header(classes, show = 1, title = f" - Ende {hd[0]}", hclassnr = 1)
                    stop = 1
                    break
                # (middle) day in holidays?
                if hd[1] < d < hd[2]:
                    date_header_obj[d] = Date_Header(classes)
                    # while summerholidays the number of schoolweeks has to be reset to 0
                    for i in range(len(shlds)):
                        if shlds[i][0] < d < shlds[i][1]:
                            sw = 0
                    stop = 1
                    break
            if stop == 1:
                continue
            # freeday?
            if d in freedays:
                date_header_obj[d] = Date_Header(classes, show = 1, title = f' - {freedays[d]}', hclassnr = 1)
                continue
            # at this point there should be only schooldays in the loop
            # conference day?
            date_header_obj[d] = Date_Header(classes)
            if is_conference_day(d, conf_day):
                date_header_obj[d].title = " - Konferenztag"
                date_header_obj[d].hclass = classes[2]
                # allways shown for the current season
                date_header_obj[d].show = 1 if d < shlds[1][0] else 0
            # start of schoolweek?
            if d.isocalendar()[1] != kw:
                kw = d.isocalendar()[1]
                sw += 1
                # odd weeknumbers are "B"-weeks, even "A"-weeks
                date_header_obj[d].title += f' - SW {sw} ({"A" if sw % 2 == 0 else "B"})'
                date_header_obj[d].hclass = classes[3]
                date_header_obj[d].show = 1

        return (date_header_obj)

##############################################################################################


    # Parameter
    # principals adress
    url = '<login-use-token-not-usernamepassword>@<caldav-address>'
    # "url"-name of calender
    usecals = ['termine-aktuell', 'klausuren']
    # event parameters to keep
    keep_param = ('DTSTART', 'DTEND', 'SUMMARY', 'CATEGORIES', 'DESCRIPTION', 'LOCATION')
    # days in the past
    days_past = 0
    # days in the future
    days_future = 1200 #730
    # conference days: list of tuples (startdate, enddate, weekday) - weekday: 0 is monday, ...
    conference_days = [((2018, 1, 1), (2019, 7, 4), 3), ((2019, 8, 14), (2021, 7, 20), 1)]
    # classes (colors) for date-headers in callist: 0: normal, 1: start or end of holidays, free day, 2: conference-day, 3: start of schoolweek
    #header_classes = ["table-light", "table-warning", "table-success", "table-info"]
    header_classes = ["table-secondary", "table-warning", "table-success", "table-info"]

    # get the right calendar
    client = caldav.DAVClient(url)
    principal = client.principal()
    calendars = principal.calendars()

    # get a dict of date_haeder_obj for date-keys
    date_header_objs = get_Date_Header_objs(conference_days, header_classes)

    # start and final day of the event-search
    start_date = datetime.date.today() - datetime.timedelta(days=days_past)
    start_date -= datetime.timedelta(days=start_date.weekday())
    end_date = datetime.date.today() + datetime.timedelta(days=days_future +1 )

    events = []

    # read events from calendars
    for c in usecals:
        # search for calendar with the right displayname
        cal = ""
        if len(calendars) > 0:
            for calendar in calendars:
                if c == calendar.url.split('/')[-2]:
                #if calendar.get_properties([dav.DisplayName(), ])['{DAV:}displayname'] == usecal:
                    cal = calendar
                    break
            else:
                print("Kalender {} nicht gefunden!".format(usecal))
                quit()


        #results = calendar.date_search(start_date, end_date)
        results = cal.date_search(start_date, end_date)

        # create a list of all events once
        for r in results:
            events.append(ics_event(r._data))
        
    # create an ordered dict
    # key is the date, values are 
    # 0: events, 1: full-dates, 2: show_date (show the dates header, even without an event.)
    # 3: text (text appending the date header), 4: hclass (table-(color)-class of the header)
    ed = OrderedDict()
    
    for d in daterange(start_date, end_date):
        # text, hclass and showdate are not set for all dates. Fill with standard values.
        if not d in date_header_objs.keys():
            date_header_objs[d] = Date_Header(header_classes)
        # for all dates we create the dict-lists: events and fulldays are filled in afterward
        ed[d] = [[], [], date_header_objs[d]]
        # check for each event if it happens on this specific day (too)
        for e in events:
            if e.is_at(d):
                if e.FULLDAY:
                    ed[d][1].append(e)
                else:
                    ed[d][0].append(e)

    # sort the sub-lists for each day
    for d in ed:
        ed[d][0].sort(key = lambda x: x.DTSTART)
        ed[d][1] = sorted(ed[d][1], key = lambda x: x.SUMMARY)


    return render_template('index.html', title="Home", ed=ed)
