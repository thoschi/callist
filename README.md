# callist

CalList (Calendar as a List) is a (little bit chaotic) Flask-based CalDav-parser that creates a list of all calendar events, ready to publish as a website.

This program is highly adapted to the needs of our school (i.e. we have a certain "conference day", highlight the holiday for lower saxony, ...).

Otherwise it might be helpful if someone tries to program his own calendar list or is just interested in caldav-parsing from python/flask.

There is still a lot of clutter within the files, as we used another project as a base - so any improvement is highly welcome.

Also if you have any ideas, just fork and enhance our code - we are still beginners and this project was mostly some kind of "yes we can".

## Preparation

The calendar informations (use tokens as usernam) have yet to be stored in `app/routes.py` line 266.

You need to (re)create the venv as defined in `requirements.txt` using 
`virtualenv --no-site-packages --distribute venv && source venv/bin/activate && pip install -r requirements.txt`


## Running

We run the script using WSGI on port 5005. We use init.d script `/etc/init.d/callist`:

```
#!/bin/bash
### BEGIN INIT INFO
# Provides:          callist
# Required-Start:    $remote_fs $network
# Required-Stop:     $remote_fs $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6	
# Short-Description: starts callist
# Description:       Starts The Callist Calendar Parser
### END INIT INFO

case "$1" in 
start)
   /opt/callist/callist &
   echo $!>/var/run/callist.pid
   ;;
stop)
   kill `cat /var/run/callist.pid`
   rm /var/run/callist.pid
   ;;
restart)
   $0 stop
   $0 start
   ;;
status)
   if [ -e /var/run/calist.pid ]; then
      echo callst is running, pid=`cat /var/run/callist.pid`
   else
      echo callist is NOT running
      exit 1
   fi
   ;;
*)
   echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0
```


## Publishing the website

Finally we use apache as reverse proxy (handles SSL too):

```
<VirtualHost *:443>
ServerName <fqdn of calendar subdomain>:443

# SSL configuration, you may want to take the easy route instead and use Lets Encrypt!
SSLEngine on
SSLCertificateFile	    <we use letsencrypt-certs here>
SSLCertificateKeyFile	<we use letsencrypt-certs here>
SSLProtocol             all -SSLv2 -SSLv3
SSLCipherSuite ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS
SSLHonorCipherOrder     on

ProxyPass           / http://127.0.0.1:5005
</VirtualHost>
```

